<?php

namespace app\controllers;

use app\models\Aboutadv;
use app\models\Aboutcomfort;
use app\models\Aboutinfo;
use app\models\Aboutsub;
use app\models\Agreement;
use app\models\Banner;
use app\models\Brand;
use app\models\Catalog;
use app\models\Category;
use app\models\Contact;
use app\models\Feedback;
use app\models\Filial;
use app\models\Mainabout;
use app\models\Mainsub;
use app\models\Menu;
use app\models\Product;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class SiteController extends FrontendController
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public function actionIndex()
    {
        $model = Menu::find()->where('url = "/"')->one();
        $this->setMeta($model->metaName, $model->metaKey, $model->metaDesc);
        $mainsub = Mainsub::find()->all();
        $banner = Banner::find()->all();
        $category = Category::find()->where('isPopular=1')->all();
        $brand = Brand::find()->all();
        $mainabout = Mainabout::find()->one();

        return $this->render('index', compact('banner', 'mainsub', 'category', 'brand', 'mainabout', 'model'));
    }


    public function actionCatalog($id, $type = 'view')
    {
        $model = Category::find()->where('id =' . $id)->one();
        $this->setMeta($model->metaName, $model->metaKey, $model->metaDesc);
        if ($type == 'view') {
            $products = Product::getAll($id);
        } elseif ($type == 'cheap') $products = Product::getOrderByCheap($id);
        elseif ($type == 'expensive') $products = Product::getOrderByExpensive($id);
        elseif ($type == 'new') $products = Product::getNew($id);
        elseif ($type == 'promo') $products = Product::getPromo($id);

        return $this->render('catalog', compact('model', 'products'));
    }


    public function actionAllCatalog($id)
    {
        $model = Category::find()->where('id =' . $id)->one();
        $products['data'] = Product::find()->where('category_id=' . $id)->orderBy('id DESC')->all();
        $this->setMeta($model->metaName, $model->metaKey, $model->metaDesc);

        return $this->render('allCatalog', compact('model', 'products'));
    }


    public function actionAbout()
    {
        $model = Menu::find()->where('url = "/site/about"')->one();
        $this->setMeta($model->metaName, $model->metaKey, $model->metaDesc);
        $about['sub'] = Aboutsub::find()->all();
        $about['info'] = Aboutinfo::find()->all();
        $about['comfort'] = Aboutcomfort::find()->one();
        $about['adv'] = Aboutadv::find()->all();

        return $this->render('about', compact('about', 'model'));
    }


    public function actionContact()
    {
        $model = Menu::find()->where('url = "/site/contact"')->one();
        $this->setMeta($model->metaName, $model->metaKey, $model->metaDesc);
        $filial = Filial::find()->all();
        $agreement = Agreement::find()->one();

        return $this->render('contact', compact('model', 'filial', 'agreement'));
    }


    public function actionRequest()
    {

        $model = new Feedback();

        $name = $_GET['name'];
        $phone = $_GET['phone'];
        $content = $_GET['content'];

        if ($model->saveRequest($name, $phone, $content)) {
            $array = ['status' => 1];
        } else {
            $array = ['status' => 0];
        }

        return json_encode($array);
    }


    public function actionRequestEmail()
    {

        $name = $_GET['name'];
        $phone = $_GET['phone'];
        $content = $_GET['content'];

        if ($this->sendRequest($name, $phone, $content)) {
            $array = ['status' => 1];
        } else {
            $array = ['status' => 0];
        }

        return json_encode($array);
    }


    public function actionTest()
    {
        $pathSyncXML = \Yii::$app->basePath . '/sync/products.xml';

        if (file_exists($pathSyncXML)) {
            $products = simplexml_load_file($pathSyncXML);
            if (count($products)) {
                foreach ($products as $attr) {
                    $price = (int)str_replace(' ', '', $attr->price);
                    $isDiscount = isset($attr->discount) && $attr->discount > 0;
                    $newPrice = 0;

                    if ($isDiscount) {
                        $newPrice = $price - ($price * ((int)$attr->discount / 100));
                    }

                    $attributes = [
                        'name' => $attr->name,
                        'volume' => "{$attr->size} л",
                        'price' => $price,
                        'company' => $attr->manufacturer,
                        'gradus' => "{$attr->percent}%",
                        'articul' => $attr->code,
                        'isDiscount' => (int)$isDiscount,
                        'newPrice' => $newPrice,
                        'promo' => $isDiscount ? $attr->discount : null,
                    ];

                    if ( ! $product = Product::find()->where(['articul' => $attr->code])->one()) {
                        $product = new Product();

                        $categoryName = (@$attr->category1) ? $attr->category1 : $attr->category;

                        $category = Category::find()->where(['name' => trim($categoryName)])->one();

                        echo '<pre>';
                        var_dump($category);
                        exit;

                        $attributes = array_merge($attributes, [
                            'country' => (isset($attr->country) && $attr->country) ? ucwords(mb_strtolower($attr->country, 'utf-8')) : null,
                            'category_id' => ($category) ? $category->id : 1,
                        ]);
                    }

                    $product->load($attributes, '');
                    $product->save(false);
                }
            }
        }
    }

}
