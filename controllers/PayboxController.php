<?php

namespace app\controllers;


use app\models\Orderedproduct;
use app\models\Ordersbypayment;
use app\models\Product;
use yii\web\Controller;
use Yii;
use Paybox\Pay\Facade as PayboxApi;

class PayboxController extends FrontendController
{
    public function actionIndex()
    {
        $id = Product::getProductID();
        $sum = Product::getSum();
        if(!Yii::$app->user->isGuest){
            $order = new Ordersbypayment();
            $order->user_id = Yii::$app->user->identity->id;
            $order->products_id = $id;
            $order->statusProgress = 0;
            $order->statusPay = 0;
            $order->date = date('y-m-d');
            $order->sum = $sum;
            $order->createdat = time();
            if($order->save()){

                foreach ($_SESSION['basket'] as $v){
                    $orderedProduct = new Orderedproduct();
                    $orderedProduct->order_id = $id;
                    $orderedProduct->product_id = $v->id;
                    $orderedProduct->count = $v->count;
                    $orderedProduct->save();
                }
                unset($_SESSION['basket']);
                $component = Yii::$app->paybox;
                $component->Index($order->id, 'Оплата заказа №'.$order->id, $order->sum, $order->getUserPhone(), $order->getUserEmail());
                return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);


//                $type = 'view';
//                Yii::$app->getSession()->setFlash('basketSuccess', 'Ваш заказ принят!');
//                return $this->redirect(['/site/cabinet','type'=>$type]);
            }else{
                Yii::$app->getSession()->setFlash('basketFailed', 'Oops, что-то пошло не так...');
                return $this->redirect(['card/basket']);
            }
        }
    }

//    public function Check()
////    {
////        $paybox = new PayboxApi();
////        echo $paybox->cancel('Ошибка');
////        die;
////    }

    public function actionResult()
    {
		//$Order = Order::findOne($_GET['pg_order_id']);
		//$Order->kod = $this->generateRandomString();
		//$Order->status = 1;
		//$Order->save(false);
        if($_GET['pg_can_reject'] == 1){
            $Order = Ordersbypayment::findOne($_GET['pg_order_id']);
            $Order->statusPay = 1;
			$Order->save();
        }else{
            $paybox = new PayboxApi();
            echo $paybox->cancel('Ошибка');
        }
        die;
    }

    public function actionSuccess()
    {
        $Order = Ordersbypayment::findOne($_GET['pg_order_id']);
		if($Order->statusPay){
            $type = 'view';
            return $this->redirect(['/site/cabinet','type'=>$type,'tab'=>'fav']);
		}
        return $this->redirect(Yii::$app->homeUrl);
    }

    public function actionFailure()
    {
        Yii::$app->getSession()->setFlash('basketFailed', 'Oops, что-то пошло не так...');
        return $this->redirect(['card/basket']);
    }


}