<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;

/**
 * Test controller
 */
class SyncController extends Controller
{

    public function uploadImages()
    {
        Yii::$app->sync->importProducts();
    }


    public function actionProducts()
    {
        Yii::$app->sync->importProducts();
    }

}