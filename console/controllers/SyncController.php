<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;

/**
 * Test controller
 */
class SyncController extends Controller
{

    public function actionImages()
    {
        Yii::$app->sync->uploadImages();
    }


    public function actionProducts()
    {
        Yii::$app->sync->importProducts();
    }

}